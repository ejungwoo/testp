#ifndef TPRANDOMPOINTCREATOR_HH
#define TPRANDOMPOINTCREATOR_HH

#include "TClonesArray.h"

#include "LKTask.hpp"
#include "LKWPoint.hpp"

class TPRandomPointCreator : public LKTask
{ 
  public:
    TPRandomPointCreator();
    virtual ~TPRandomPointCreator() {}

    bool Init();
    void Exec(Option_t*);

  private:
    TClonesArray* fPointArray = nullptr;

    Int_t fNumPoints = 100;

  ClassDef(TPRandomPointCreator, 0)
};

#endif

