#ifndef TPRANDOMGAUSCREATOR_HH
#define TPRANDOMGAUSCREATOR_HH

#include "TClonesArray.h"

#include "LKTask.hpp"
#include "LKWPoint.hpp"

class TPRandomGausCreator : public LKTask
{ 
  public:
    TPRandomGausCreator();
    virtual ~TPRandomGausCreator() {}

    bool Init();
    void Exec(Option_t*);

  private:
    TClonesArray* fPointArray = nullptr;
    TClonesArray* fGausArray = nullptr;

  ClassDef(TPRandomGausCreator, 0)
};

#endif
